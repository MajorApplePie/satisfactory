import { Recipe } from './recipe';

export interface Material {
  name: string;
  image?: string;
  recipes?: Recipe[];
}
