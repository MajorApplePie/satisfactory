export interface Recipe {
  building: string;
  duration: number;
  materialNeeds: MaterialNeed[];
  resultingAmount: number;
}


export interface MaterialNeed {
  amount: number;
  name: string;
}
