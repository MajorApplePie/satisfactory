import { Recipe, MaterialNeed } from './recipe';

export interface Building {
  name: string;
  powerConsumption: number;
  ingredients: MaterialNeed[];
  recipes: Recipe[];
  width: number;
  height: number;
  length: number;
  inputs: Port[];
  outputs: Port[];
}

export interface Port {
  name: string;
  amount: number;
}
